/*!
 * Copyright (C) 2000-2016 Silverpeas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * As a special exception to the terms and conditions of version 3.0 of
 * the GPL, you may redistribute this Program in connection with Writer Free/Libre
 * Open Source Software ("FLOSS") applications as described in Silverpeas's
 * FLOSS exception.  You should have recieved a copy of the text describing
 * the FLOSS exception, and it is also available here:
 * "http://www.silverpeas.org/legal/licensing"
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

jsxcLanguageResources.fr.translation['ClearHistory'] = 'Effacer l\'historique local des conversations';
jsxcLanguageResources.en.translation['ClearHistory'] = 'Clear the talk history';
jsxcLanguageResources.de.translation['ClearHistory'] = 'Löschen Sie den Diskussionsverlauf';

jsxcLanguageResources.fr.translation['Buddies'] = 'Contacts';
jsxcLanguageResources.en.translation['Buddies'] = 'Buddies';
jsxcLanguageResources.de.translation['Buddies'] = 'Kontakte';

jsxcLanguageResources.fr.translation['Group Chats'] = 'Discussions';
jsxcLanguageResources.en.translation['Group Chats'] = 'Group Chats';
jsxcLanguageResources.de.translation['Group Chats'] = 'Gruppe Chat';

jsxcLanguageResources.fr.translation['Group_Chat'] = 'Discussion de groupe';
jsxcLanguageResources.en.translation['Group_Chat'] = 'Group Chat';
jsxcLanguageResources.de.translation['Group_Chat'] = 'Gruppe Chat';

jsxcLanguageResources.fr.translation['Menu'] = 'Menu';
jsxcLanguageResources.en.translation['Menu'] = 'Menu';
jsxcLanguageResources.de.translation['Menu'] = 'Menu';

jsxcLanguageResources.fr.translation['Search_user'] = 'Rechercher un utilisateur';
jsxcLanguageResources.en.translation['Search_user'] = 'Looking for a user';
jsxcLanguageResources.de.translation['Search_user'] = 'Benutzer ein suchen';

jsxcLanguageResources.fr.translation['Notifications'] = 'Notifications';
jsxcLanguageResources.en.translation['Notifications'] = 'Notifications';
jsxcLanguageResources.de.translation['Notifications'] = 'Benachrichtigungen';

jsxcLanguageResources.fr.translation['New_Chat'] = 'Nouvelle discussion';
jsxcLanguageResources.en.translation['New_Chat'] = 'New chat';
jsxcLanguageResources.de.translation['New_Chat'] = 'Neuer Chat';

jsxcLanguageResources.fr.translation['New_Group_Chat'] = 'Nouvelle discussion de groupe';
jsxcLanguageResources.en.translation['New_Group_Chat'] = 'New group chat';
jsxcLanguageResources.de.translation['New_Group_Chat'] = 'Neuer Gruppe Chat';

jsxcLanguageResources.fr.translation['Invite_To_Group_Chat'] = 'Inviter dans la discussion de groupe';
jsxcLanguageResources.en.translation['Invite_To_Group_Chat'] = 'Invite to the group chat';
jsxcLanguageResources.de.translation['Invite_To_Group_Chat'] = 'Laden Sie zum Gruppe Chat ein';

jsxcLanguageResources.fr.translation['New_invitation'] = '__sender__ vous invite à rejoindre la discussion de groupe __room__';
jsxcLanguageResources.en.translation['New_invitation'] = '__sender__ invite you to join him the group chat __room__';
jsxcLanguageResources.de.translation['New_invitation'] = '__sender__ lädt Sie ein, sich dem Gruppe Chat __room__ anzuschließen';

jsxcLanguageResources.fr.translation['Chat_room_name_pattern'] = 'Pas d\'espaces, pas de caractères accentués ou spéciaux (&"\'/:,;!?<>@)';
jsxcLanguageResources.en.translation['Chat_room_name_pattern'] = 'No spaces, no accented or special characters (&"\'/:,;!?<>@)';
jsxcLanguageResources.de.translation['Chat_room_name_pattern'] = 'Keine Leerzeichen, keine akzentuierten oder Sonderzeichen (&"\'/:,;!? <> @)';

jsxcLanguageResources.fr.translation['Search_User'] = 'Rechercher un utilisateur par son identifiant';
jsxcLanguageResources.en.translation['Search_User'] = 'Search a user by its identifier';
jsxcLanguageResources.de.translation['Search_User'] = 'Suchen Sie einen Benutzer nach seinem Bezeichner';

jsxcLanguageResources.fr.translation['You_have_received_message_from_'] = 'Vous avez reçu un message de ';
jsxcLanguageResources.en.translation['You_have_received_message_from_'] = 'You have received a message from ';
jsxcLanguageResources.de.translation['You_have_received_message_from_'] = 'Du hast eine Nachricht von ';

jsxcLanguageResources.fr.translation['Events'] = 'Événements';
jsxcLanguageResources.en.translation['Events'] = 'Events';
jsxcLanguageResources.de.translation['Events'] = 'Ereignisse';

jsxcLanguageResources.fr.translation['message_not_send_resource-unavailable'] = "Votre message n'a pas été envoyé parce que votre interlocuteur n'est pas connecté ou accessible";

// From this line, override the translations of JSXC by our own

jsxcLanguageResources.fr.translation['left_the_building'] = "__nickname__ a quitté le groupe de discussion";
jsxcLanguageResources.en.translation['left_the_building'] = "__nickname__ left the chat group";
jsxcLanguageResources.de.translation['left_the_building'] = "__nickname__ hat die Gruppe verlassen";

jsxcLanguageResources.fr.translation['entered_the_room'] = "__nickname__ entre dans le groupe de discussion";
jsxcLanguageResources.en.translation['entered_the_room'] = "__nickname__ entered the chat group";
jsxcLanguageResources.de.translation['entered_the_room'] = "__nickname__ ist der Gruppe beigetreten";